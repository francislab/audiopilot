%micCalibration is a program that uses the output of the Analog Signal
%Recorder App in Matlab to check the voltage output of the microphone
%against an expected value (Vref) from the manufacturer. Currently, in the
%Francis lab we use the Bruer & Kaer Type 4138 microphone with an expected
%Vref = 9.0400e-04 V/Pa, connected via preamp to a B&K Type 1708 amplifier
%set to VPol (200V), Lin., x100, and CIC (off). The calibration can be done
%by plugging ampifier BNC output to the Psystem CCU analog input, or any
%other NI analog input.

%After setting up microhpone in calibrating speaker (eg., B&K Type 4231
%that outputs 94 dB SPL (1 pA) @ 1 kHz), run the Analog Signal Recorder app
%and set the scan rate to 200000 and workspace variable name to 'calb'. For
%the Psystem CCU, Set the correct input channel, use 'SingleEnded'
%measurement type, and +/- 1V. Record for 6 seconds with calibration system
%ON.

%Variables
saveDir = 'c:\audioPilot'; %saving directory
fs=200000; %sampling rate
ampGain=100; %amplifier gain setting
Vref=ampGain*9.0400e-04; %amplifier gain setting * expected V/Pa
Pref=20e-6; %pressure reference, 20uPa
varname = 'calb';
device = 'Dev1';
aichannel = 'ai4';

%Aquire data from Analog Signal Recorder App
a=eval([varname '.' device '_' aichannel]); %the strucutre var name will depend on the NI device and ai channel number.
n=length(a);
b=a(2500:2500+(fs));
ramp = hanning(round(.2 * fs*2));
ramp = ramp(1:floor(length(ramp)/2));
b(1:length(ramp)) = b(1:length(ramp)) .* ramp;
b(end-length(ramp)+1:end) = b(end-length(ramp)+1:end) .* flipud(ramp);

%Filter 1 kHz data
[s m]=butter(3,[500 1500]./(fs/2),'bandpass');
c=filtfilt(s,m,b);
Vrms = round(rms(c),3);

%Plot 1kHz waveform
figure
t=0:1/fs:((length(c)./fs))-(1/fs);
plot(t,c)
ylabel('Volts')
xlabel('Time (s)')
title('1kHz Calibration Waveform')

%Obtain rms(Pa) = rms(V/(V/Pa))
PaRMS = rms(c./Vref);

%Calculate recorded dB SPL
dB=20*log10(PaRMS./Pref);

clc
disp(['94 dB SPL expected; ' num2str(round(dB)) ' dB SPL obtained'])
disp(['Vref = ' num2str(Vrms) 'V/Pa'])

s=input(['Save Vref? (y/n): '],'s');
if strcmpi(s,'y')
    save([saveDir '\micCalb.mat'],'Vrms');
end
    

