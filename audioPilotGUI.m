function varargout = audioPilotGUI(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @audioPilotGUI_OpeningFcn, ...
    'gui_OutputFcn',  @audioPilotGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% --- Executes just before audioPilotGUI is made visible.
function audioPilotGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% sets a variable to be used in graphing and stopping devices
handles.Stop = 0;

%sets file path to the current file that Matlab has open
fileLocation = pwd;
set(handles.fileLocation,'String',fileLocation);

%sets the target and nontarget tone selections to be empty
handles.target = [];
handles.nontarget = [];

%logs selected and running device
handles.onDevices = {};

%to avoid retyping during debugging
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% handles.piSelection.String = '192.168.1.209';
% handles.cageID.String = 'audiopilot01';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5555

% disable GUI options
set(handles.piSelection,'Enable','off')
set(handles.cageID,'Enable','off')
set(handles.connectPi,'Enable','off')
set(handles.stimulus,'Enable','off');
set(handles.speakerCalibration,'Enable','off');
set(handles.waterTest,'Enable','off');
set(handles.testLick,'Enable','off');
set(handles.testStim,'Enable','off');
set(handles.previousParams,'Enable','off');
set(handles.startDelay,'Enable','off');
set(handles.trialBlockDur,'Enable','off');
set(handles.cageID,'Enable','off');
set(handles.startButton,'Enable','off')
set(handles.setParams,'Enable','off')
set(handles.phaseSelection,'Enable','off');
set(handles.tone1,'Enable','off')
set(handles.tone2,'Enable','off')
set(handles.tone3,'Enable','off')
set(handles.tone4,'Enable','off')
set(handles.tone5,'Enable','off')
set(handles.tone6,'Enable','off')
set(handles.tone7,'Enable','off')
set(handles.tone8,'Enable','off')
set(handles.tone9,'Enable','off')
set(handles.tone10,'Enable','off')
set(handles.toneNT1,'Enable','off')
set(handles.toneNT2,'Enable','off')
set(handles.toneNT3,'Enable','off')
set(handles.toneNT4,'Enable','off')
set(handles.toneNT5,'Enable','off')
set(handles.toneNT6,'Enable','off')
set(handles.toneNT7,'Enable','off')
set(handles.toneNT8,'Enable','off')
set(handles.toneNT9,'Enable','off')
set(handles.toneNT10,'Enable','off')
set(handles.catchTrials,'Enable','off')
set(handles.catchTrials,'Value',0)
set(handles.level1,'Enable','off')
set(handles.level2,'Enable','off')
set(handles.level3,'Enable','off')
set(handles.level4,'Enable','off')
set(handles.level5,'Enable','off')
set(handles.level6,'Enable','off')
set(handles.level7,'Enable','off')

% Choose default command line output for audioPilotGUI (provided by gui code)
handles.output = hObject;

guidata(hObject,handles)
uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = audioPilotGUI_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles;

%file location is the textbox that you type the file path into
function fileLocation_Callback(hObject, eventdata, handles)
function fileLocation_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in filePath. Saves file path entered into the file location text box
function filePath_Callback(hObject, eventdata, handles)
% checks to see if file path is valid, if not then a warning message popsup
checkPath1 = exist(handles.fileLocation.String);
if checkPath1 == 0
    popup = msgbox('Invalid file path');
else
    
    % checks to see if a Devices folder already exists in this location
    checkPath2 = exist([handles.fileLocation.String '\Devices']);
    checkPath3 = exist([handles.fileLocation.String 'Devices']);
    
    % if the folder doesn't exist then a dialogue box opens and asks user if
    % this is the correct file path, if so then a new Devices folder will
    % be made at that locaiton, if not then the user will input a new
    % filepath
    if checkPath2 == 0 && checkPath3 == 0
        answer = questdlg('A Devices folder does not yet exist in this location. Is this the correct file path?',...
            'File Path','Yes, make new Devices folder','No, change file path','Yes, make new Devices folder');
        switch answer
            case 'Yes, make new Devices folder'
                mkdir([handles.fileLocation.String '\Devices\'])
            case 'No, change file path'
                
        end
    end
end

%saves the devices folder
handles.devicesFolder = [handles.fileLocation.String '\Devices\'];

% changes variable name so it can be saved and passed to the batch job as an argument
devicesFolder = handles.devicesFolder;

% enable buttons
set(handles.connectPi,'Enable','on')
set(handles.piSelection,'Enable','on')
set(handles.cageID,'Enable','on')

guidata(hObject,handles);

% --- Executes on button press in connect PI
function connectPi_Callback(hObject, eventdata, handles)
if ~isempty(handles.piSelection.String)
    if ~isempty(handles.cageID.String)
        % connects selected raspberry pi to matlab
        piConnect=0;
        try
            rpi = raspi(handles.piSelection.String,'audiopilot','thirstymouse');
            piConnect=1;
            
        end
        
        if piConnect
            % deletes local variable which was saved to the parameters file
            handles.rpi = rpi;
            clear rpi;
            
            % sets file path for where parameters will be saved
            handles.filename = [handles.devicesFolder 'PiParams.mat'];
            
            %update button color/text
            handles.connectPi.BackgroundColor = [0 1 0];
            handles.connectPi.String = 'Pi Connected';
            
            %enable gui options
            set(handles.stimulus,'Enable','on');
            set(handles.speakerCalibration,'Enable','on');
            set(handles.waterTest,'Enable','on');
            set(handles.testLick,'Enable','on');
            set(handles.testStim,'Enable','on')
            set(handles.previousParams,'Enable','on');
            set(handles.phaseSelection,'Enable','on');
            set(handles.connectPi,'Enable','off');
            set(handles.startDelay,'Enable','on');
            set(handles.trialBlockDur,'Enable','on');
            
        else
            handles.connectPi.BackgroundColor = [1 0 0];
            warndlg('No connection. Restart Matlab and hardware')
            
        end
    end
else
    warndlg('Enter a Cage ID')
    
end

guidata(hObject,handles);

function connectPi_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function piSelection_Callback(hObject, eventdata, handles)
function piSelection_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Phase that will be run
function phaseSelection_Callback(hObject, eventdata, handles)
%checks the string of the hObject's current value
str = get(hObject, 'String');
val = get(hObject, 'Value');
tarVal = (get(handles.stimulus,'Value'));
tarStr = handles.stimulus.String{tarVal};
switch str{val}
    
    case 'Training Phase:'
        % No phase has been selected so all the buttons and boxes remain disabled
        handles.phaseChoice = 0;
        set(handles.startButton,'Enable','off');
        set(handles.setParams,'Enable','off');
        set(handles.tone1,'Enable','off');
        set(handles.tone2,'Enable','off');
        set(handles.tone3,'Enable','off');
        set(handles.tone4,'Enable','off');
        set(handles.tone5,'Enable','off');
        set(handles.tone6,'Enable','off');
        set(handles.tone7,'Enable','off');
        set(handles.tone8,'Enable','off');
        set(handles.tone9,'Enable','off');
        set(handles.tone10,'Enable','off');
        set(handles.toneNT1,'Enable','off');
        set(handles.toneNT2,'Enable','off');
        set(handles.toneNT3,'Enable','off');
        set(handles.toneNT4,'Enable','off');
        set(handles.toneNT5,'Enable','off');
        set(handles.toneNT6,'Enable','off');
        set(handles.toneNT7,'Enable','off');
        set(handles.toneNT8,'Enable','off');
        set(handles.toneNT9,'Enable','off');
        set(handles.toneNT10,'Enable','off');
        set(handles.tone1, 'Value', 0);
        set(handles.tone2, 'Value', 0);
        set(handles.tone3, 'Value', 0);
        set(handles.tone4, 'Value', 0);
        set(handles.tone5, 'Value', 0);
        set(handles.tone6, 'Value', 0);
        set(handles.tone7, 'Value', 0);
        set(handles.tone8, 'Value', 0);
        set(handles.tone9, 'Value', 0);
        set(handles.tone10, 'Value', 0);
        set(handles.toneNT1, 'Value', 0);
        set(handles.toneNT2, 'Value', 0);
        set(handles.toneNT3, 'Value', 0);
        set(handles.toneNT4, 'Value', 0);
        set(handles.toneNT5, 'Value', 0);
        set(handles.toneNT6, 'Value', 0);
        set(handles.toneNT7, 'Value', 0);
        set(handles.toneNT8, 'Value', 0);
        set(handles.toneNT9, 'Value', 0);
        set(handles.toneNT10, 'Value', 0);
        set(handles.level1,'Enable','off')
        set(handles.level2,'Enable','off')
        set(handles.level3,'Enable','off')
        set(handles.level4,'Enable','off')
        set(handles.level5,'Enable','off')
        set(handles.level6,'Enable','off')
        set(handles.level7,'Enable','off')
        set(handles.level1,'Value',0)
        set(handles.level2,'Value',0)
        set(handles.level3,'Value',0)
        set(handles.level4,'Value',0)
        set(handles.level5,'Value',0)
        set(handles.level6,'Value',0)
        set(handles.level7,'Value',0)
        set(handles.catchTrials,'Enable','off')
        set(handles.catchTrials,'Value',0)
        
    case 'Shaping'
        handles.phaseChoice = 1;
        
        % "Set Params" button is enabled
        set(handles.setParams,'Enable','on');
        
        if ~strcmpi(tarStr,'LED')
            % Target tone checkboxes are enabled
            set(handles.tone1,'Enable','on');
            set(handles.tone2,'Enable','on');
            set(handles.tone3,'Enable','on');
            set(handles.tone4,'Enable','on');
            set(handles.tone5,'Enable','on');
            set(handles.tone6,'Enable','on');
            set(handles.tone7,'Enable','on');
            set(handles.tone8,'Enable','on');
            set(handles.tone9,'Enable','on');
            set(handles.tone10,'Enable','on');
        end
        
        % Nontarget tone checkboxes remain disabled because shaping does not use them
        set(handles.toneNT1,'Enable','off');
        set(handles.toneNT2,'Enable','off');
        set(handles.toneNT3,'Enable','off');
        set(handles.toneNT4,'Enable','off');
        set(handles.toneNT5,'Enable','off');
        set(handles.toneNT6,'Enable','off');
        set(handles.toneNT7,'Enable','off');
        set(handles.toneNT8,'Enable','off');
        set(handles.toneNT9,'Enable','off');
        set(handles.toneNT10,'Enable','off');
        set(handles.toneNT1, 'Value', 0);
        set(handles.toneNT2, 'Value', 0);
        set(handles.toneNT3, 'Value', 0);
        set(handles.toneNT4, 'Value', 0);
        set(handles.toneNT5, 'Value', 0);
        set(handles.toneNT6, 'Value', 0);
        set(handles.toneNT7, 'Value', 0);
        set(handles.toneNT8, 'Value', 0);
        set(handles.toneNT9, 'Value', 0);
        set(handles.toneNT10, 'Value', 0);
        
        %disables options to select tone levels
        set(handles.level1,'Enable','off')
        set(handles.level2,'Enable','off')
        set(handles.level3,'Enable','off')
        set(handles.level4,'Enable','off')
        set(handles.level5,'Enable','off')
        set(handles.level6,'Enable','off')
        set(handles.level7,'Enable','off')
        set(handles.level1,'Value',0)
        set(handles.level2,'Value',0)
        set(handles.level3,'Value',0)
        set(handles.level4,'Value',0)
        set(handles.level5,'Value',0)
        set(handles.level6,'Value',0)
        set(handles.level7,'Value',0)
        
        %disables catch trials option
        set(handles.catchTrials,'Enable','off')
        set(handles.catchTrials,'Value',0)
        
    case 'Detection'
        handles.phaseChoice = 2;
        
        % "Set Params" button is enabled
        set(handles.setParams,'Enable','on');
        
        if ~strcmpi(tarStr,'LED')
            %Enable gui options
            set(handles.tone1,'Enable','on');
            set(handles.tone2,'Enable','on');
            set(handles.tone3,'Enable','on');
            set(handles.tone4,'Enable','on');
            set(handles.tone5,'Enable','on');
            set(handles.tone6,'Enable','on');
            set(handles.tone7,'Enable','on');
            set(handles.tone8,'Enable','on');
            set(handles.tone9,'Enable','on');
            set(handles.tone10,'Enable','on');
            set(handles.level1,'Enable','on')
            set(handles.level2,'Enable','on')
            set(handles.level3,'Enable','on')
            set(handles.level4,'Enable','on')
            set(handles.level5,'Enable','on')
            set(handles.level6,'Enable','on')
            set(handles.level7,'Enable','on')
        end
        
        %enables option for catch trials
        set(handles.catchTrials,'Enable','on')
        
        % Nontarget tone checkboxes remain disabled because detection does not use them
        set(handles.toneNT1,'Enable','off');
        set(handles.toneNT2,'Enable','off');
        set(handles.toneNT3,'Enable','off');
        set(handles.toneNT4,'Enable','off');
        set(handles.toneNT5,'Enable','off');
        set(handles.toneNT6,'Enable','off');
        set(handles.toneNT7,'Enable','off');
        set(handles.toneNT8,'Enable','off');
        set(handles.toneNT9,'Enable','off');
        set(handles.toneNT10,'Enable','off');
        set(handles.toneNT1, 'Value', 0);
        set(handles.toneNT2, 'Value', 0);
        set(handles.toneNT3, 'Value', 0);
        set(handles.toneNT4, 'Value', 0);
        set(handles.toneNT5, 'Value', 0);
        set(handles.toneNT6, 'Value', 0);
        set(handles.toneNT7, 'Value', 0);
        set(handles.toneNT8, 'Value', 0);
        set(handles.toneNT8, 'Value', 0);
        set(handles.toneNT9, 'Value', 0);
        set(handles.toneNT10, 'Value', 0);
        
    case 'Discrimination'
        handles.phaseChoice = 3;
        
        % "Set Params" button is enabled
        set(handles.setParams,'Enable','on');
        
        if ~strcmpi(tarStr,'LED')
            %Enable gui options
            set(handles.tone1,'Enable','on')
            set(handles.tone2,'Enable','on')
            set(handles.tone3,'Enable','on')
            set(handles.tone4,'Enable','on')
            set(handles.tone5,'Enable','on')
            set(handles.tone6,'Enable','on')
            set(handles.tone7,'Enable','on')
            set(handles.tone8,'Enable','on')
            set(handles.tone9,'Enable','on');
            set(handles.tone10,'Enable','on');
            set(handles.toneNT1,'Enable','on')
            set(handles.toneNT2,'Enable','on')
            set(handles.toneNT3,'Enable','on')
            set(handles.toneNT4,'Enable','on')
            set(handles.toneNT5,'Enable','on')
            set(handles.toneNT6,'Enable','on')
            set(handles.toneNT7,'Enable','on')
            set(handles.toneNT8,'Enable','on')
            set(handles.toneNT9,'Enable','on');
            set(handles.toneNT10,'Enable','on');
            set(handles.level1,'Enable','on')
            set(handles.level2,'Enable','on')
            set(handles.level3,'Enable','on')
            set(handles.level4,'Enable','on')
            set(handles.level5,'Enable','on')
            set(handles.level6,'Enable','on')
            set(handles.level7,'Enable','on')
        end
        
        %enables option for catch trials
        set(handles.catchTrials,'Enable','on')
end

guidata(hObject,handles)

function phaseSelection_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% If one tone is selected in the targets/nontargets tone box, then the same tone is disabled in the nontargets/targets tone box
function toneNT1_Callback(hObject, eventdata, handles)
set(handles.tone1, 'Value', 0);
function toneNT2_Callback(hObject, eventdata, handles)
set(handles.tone2, 'Value', 0);
function toneNT3_Callback(hObject, eventdata, handles)
set(handles.tone3, 'Value', 0);
function toneNT4_Callback(hObject, eventdata, handles)
set(handles.tone4, 'Value', 0);
function toneNT6_Callback(hObject, eventdata, handles)
set(handles.tone6, 'Value', 0);
function toneNT7_Callback(hObject, eventdata, handles)
set(handles.tone7, 'Value', 0);
function toneNT8_Callback(hObject, eventdata, handles)
set(handles.tone8, 'Value', 0);
function toneNT5_Callback(hObject, eventdata, handles)
set(handles.tone5, 'Value', 0);
function toneNT10_Callback(hObject, eventdata, handles)
set(handles.tone10, 'Value', 0);
function toneNT9_Callback(hObject, eventdata, handles)
set(handles.tone9, 'Value', 0);
function tone1_Callback(hObject, eventdata, handles)
set(handles.toneNT1, 'Value', 0);
function tone2_Callback(hObject, eventdata, handles)
set(handles.toneNT2, 'Value', 0);
function tone3_Callback(hObject, eventdata, handles)
set(handles.toneNT3, 'Value', 0);
function tone4_Callback(hObject, eventdata, handles)
set(handles.toneNT4, 'Value', 0);
function tone6_Callback(hObject, eventdata, handles)
set(handles.toneNT6, 'Value', 0);
function tone7_Callback(hObject, eventdata, handles)
set(handles.toneNT7, 'Value', 0);
function tone8_Callback(hObject, eventdata, handles)
set(handles.toneNT8, 'Value', 0);
function tone5_Callback(hObject, eventdata, handles)
set(handles.toneNT5, 'Value', 0);
function tone10_Callback(hObject, eventdata, handles)
set(handles.toneNT10, 'Value', 0);
function tone9_Callback(hObject, eventdata, handles)
set(handles.toneNT9, 'Value', 0);

%select levels that the tones will be played at
function level5_Callback(hObject, eventdata, handles)
function level6_Callback(hObject, eventdata, handles)
function level2_Callback(hObject, eventdata, handles)
function level7_Callback(hObject, eventdata, handles)
function level4_Callback(hObject, eventdata, handles)
function level3_Callback(hObject, eventdata, handles)
function level1_Callback(hObject, eventdata, handles)

%select whether catch trials will be present
function catchTrials_Callback(hObject, eventdata, handles)
guidata(hObject,handles);

% visual check for the user so that they are happy with their selections
% before they hit the "Start" button, saves the parameters to a file that
% will be used in whichever phase program is selected, an additional file
% is saved with the parameters so that the gui can recall the previous
% parameters
function setParams_Callback(hObject, eventdata, handles)
% sets these vectors to be empty, deleting previous selections
handles.target = [];
handles.nontarget = [];
handles.toneLevel = [];

%sets variables to be saved to parameters file
targetChoice = [];
nontargetChoice = [];
toneLevelChoice = [];
catchTrials = 0;

%store trial block durations
startDelay = str2num(handles.startDelay.String);
trialBlockDur = str2num(handles.trialBlockDur.String);
trialBlockInterval = (60*24)-trialBlockDur;
save(handles.filename,'trialBlockDur','startDelay','trialBlockInterval')

stim = handles.stimulus.String{handles.stimulus.Value};
if strcmpi(stim,'Tone')
    F= round([1000.*2.^[0:.5:4.5]]./1000,1);
    % adds value of selected target to associated target vector
    if get(handles.tone1, 'Value') == 1
        handles.target = [handles.target; F(1)];
    end
    if get(handles.tone2, 'Value') == 1
        handles.target = [handles.target; F(2)];
    end
    if get(handles.tone3, 'Value') == 1
        handles.target = [handles.target; F(3)];
    end
    if get(handles.tone4, 'Value') == 1
        handles.target = [handles.target; F(4)];
    end
    if get(handles.tone5, 'Value') == 1
        handles.target = [handles.target; F(5)];
    end
    if get(handles.tone6, 'Value') == 1
        handles.target = [handles.target; F(6)];
    end
    if get(handles.tone7, 'Value') == 1
        handles.target = [handles.target; F(7)];
    end
    if get(handles.tone8, 'Value') == 1
        handles.target = [handles.target; F(8)];
    end
    if get(handles.tone9, 'Value') == 1
        handles.target = [handles.target; F(9)];
    end
    if get(handles.tone10, 'Value') == 1
        handles.target = [handles.target; F(10)];
    end
else
    handles.target = [handles.target; 'LED'];
end

% saves targets vector to the parameters file
targetChoice = handles.target;
save(handles.filename,'targetChoice','-append')

% adds value of selected nontarget to associated nontarget vector
if get(handles.toneNT1, 'Value') == 1
    handles.nontarget = [handles.nontarget; F(1)];
end
if get(handles.toneNT2, 'Value') == 1
    handles.nontarget = [handles.nontarget; F(2)];
end
if get(handles.toneNT3, 'Value') == 1
    handles.nontarget = [handles.nontarget; F(3)];
end
if get(handles.toneNT4, 'Value') == 1
    handles.nontarget = [handles.nontarget; F(4)];
end
if get(handles.toneNT5, 'Value') == 1
    handles.nontarget = [handles.nontarget; F(5)];
end
if get(handles.toneNT6, 'Value') == 1
    handles.nontarget = [handles.nontarget; F(6)];
end
if get(handles.toneNT7, 'Value') == 1
    handles.nontarget = [handles.nontarget; F(7)];
end
if get(handles.toneNT8, 'Value') == 1
    handles.nontarget = [handles.nontarget; F(8)];
end
if get(handles.toneNT9, 'Value') == 1
    handles.nontarget = [handles.nontarget; F(9)];
end
if get(handles.toneNT10, 'Value') == 1
    handles.nontarget = [handles.nontarget; F(10)];
end

% saves nontarget tones vector to the parameters file
nontargetChoice = handles.nontarget;
save(handles.filename,'nontargetChoice','-append')

% adds value of selected tone level to associated level vector
if get(handles.level1, 'Value') == 1
    handles.toneLevel = [handles.toneLevel; 0];
end
if get(handles.level2, 'Value') == 1
    handles.toneLevel = [handles.toneLevel; -5];
end
if get(handles.level3, 'Value') == 1
    handles.toneLevel = [handles.toneLevel; -10];
end
if get(handles.level4, 'Value') == 1
    handles.toneLevel = [handles.toneLevel; -15];
end
if get(handles.level5, 'Value') == 1
    handles.toneLevel = [handles.toneLevel; -20];
end
if get(handles.level6, 'Value') == 1
    handles.toneLevel = [handles.toneLevel; -25];
end
if get(handles.level7, 'Value') == 1
    handles.toneLevel = [handles.toneLevel; -30];
end

% saves tone level vector to the parameters file
toneLevelChoice = handles.toneLevel;
save(handles.filename,'toneLevelChoice','-append')

% catch trial option saved to parameters file
if get(handles.catchTrials,'Value') == 1
    catchTrials = 1;
end
save(handles.filename,'catchTrials','-append')

%resaves value to a variable that can be saved into the parameters file
phaseChoice = handles.phaseChoice;

% makes sure that a folder exists for the device that is running, if a folder doesn't exist then one is created
checkFolder = exist([handles.devicesFolder, char(handles.cageID.String)]);
if checkFolder == 0
    mkdir(handles.devicesFolder, char(handles.cageID.String))
end

%saves information to the previous parameters file for future use
save([handles.devicesFolder, char(handles.cageID.String), '\PreviousPiParams.mat'],'phaseChoice', ...
    'targetChoice', 'nontargetChoice','toneLevelChoice','catchTrials')

if strcmpi(stim,'Tone')
    %for shaping and detection
    if handles.phaseChoice == 1 || handles.phaseChoice == 2
        % if the button gets pressed before tones are selected for the task
        % then an eror message pops up
        if get(handles.tone8, 'Value') == 0 && get(handles.tone7, 'Value') == 0 ...
                && get(handles.tone6, 'Value') == 0 && get(handles.tone5, 'Value') == 0 ...
                && get(handles.tone4, 'Value') == 0 && get(handles.tone3, 'Value') == 0 ...
                && get(handles.tone2, 'Value') == 0 && get(handles.tone1, 'Value') == 0 ...
                && get(handles.tone9, 'Value') == 0 && get(handles.tone10, 'Value') == 0 ...
                popup = msgbox('No targets were selected');
            set(handles.startButton,'Enable','off')
        else
            % enables the "Start" button
            set(handles.startButton,'Enable','on')
        end
    end
    
    %for detection
    if handles.phaseChoice == 2
        %if set params button gets pressed before levels get selected then a
        %pop up message will ask the person to select a tone level
        if get(handles.level1,'Value') == 0 && get(handles.level2,'Value') == 0 ...
                && get(handles.level3,'Value') == 0 && get(handles.level4,'Value') == 0 ...
                && get(handles.level5,'Value') == 0 && get(handles.level6,'Value') == 0 ...
                && get(handles.level7,'Value') == 0
            popup = msgbox('No tone levels were selected');
            set(handles.startButton,'Enable','off')
        else
            %enables the "Start" button
            set(handles.startButton,'Enable','on')
        end
    end
    
    %for discrimination
    if handles.phaseChoice == 3
        % if the button gets pressed before tones are selected for the task
        % then an eror message pops up (for targets, nontargets, or both)
        if get(handles.toneNT8, 'Value') == 0 && get(handles.toneNT7, 'Value') == 0 ...
                && get(handles.toneNT6, 'Value') == 0 && get(handles.toneNT5, 'Value') == 0 ...
                && get(handles.toneNT4, 'Value') == 0 && get(handles.toneNT3, 'Value') == 0 ...
                && get(handles.toneNT2, 'Value') == 0 && get(handles.toneNT1, 'Value') == 0 ...
                && get(handles.toneNT9, 'Value') == 0 && get(handles.toneNT10, 'Value') == 0 ...
                && get(handles.tone8, 'Value') == 0 && get(handles.tone7, 'Value') == 0 ...
                && get(handles.tone6, 'Value') == 0 && get(handles.tone5, 'Value') == 0 ...
                && get(handles.tone4, 'Value') == 0 && get(handles.tone3, 'Value') == 0 ...
                && get(handles.tone2, 'Value') == 0 && get(handles.tone1, 'Value') == 0 ...
                && get(handles.tone9, 'Value') == 0 && get(handles.tone10, 'Value') == 0
            popup = msgbox('No targets or non-targets were selected');
            set(handles.startButton,'Enable','off')
        elseif get(handles.toneNT8, 'Value') == 0 && get(handles.toneNT7, 'Value') == 0 ...
                && get(handles.toneNT6, 'Value') == 0 && get(handles.toneNT5, 'Value') == 0 ...
                && get(handles.toneNT4, 'Value') == 0 && get(handles.toneNT3, 'Value') == 0 ...
                && get(handles.toneNT2, 'Value') == 0 && get(handles.toneNT1, 'Value') == 0 ...
                && get(handles.toneNT9, 'Value') == 0 && get(handles.toneNT10, 'Value') == 0                popup = msgbox('No non-targets were selected');
            set(handles.startButton,'Enable','off')
        elseif get(handles.tone8, 'Value') == 0 && get(handles.tone7, 'Value') == 0 ...
                && get(handles.tone6, 'Value') == 0 && get(handles.tone5, 'Value') == 0 ...
                && get(handles.tone4, 'Value') == 0 && get(handles.tone3, 'Value') == 0 ...
                && get(handles.tone2, 'Value') == 0 && get(handles.tone1, 'Value') == 0 ...
                && get(handles.tone9, 'Value') == 0 && get(handles.tone10, 'Value') == 0            popup = msgbox('No targets were selected');
            set(handles.startButton,'Enable','off')
            %if set params button gets pressed before levels get selected then a
            %pop up message will ask the person to select a tone level
        elseif get(handles.level1,'Value') == 0 && get(handles.level2,'Value') == 0 ...
                && get(handles.level3,'Value') == 0 && get(handles.level4,'Value') == 0 ...
                && get(handles.level5,'Value') == 0 && get(handles.level6,'Value') == 0 ...
                && get(handles.level7,'Value') == 0
            popup = msgbox('No tone levels were selected');
            set(handles.startButton,'Enable','off')
        else
            % enables the "Start" button
            pause(1)
            set(handles.startButton,'Enable','on')
        end
    end
    
elseif strcmpi(stim,'LED') && handles.phaseChoice ~= 3 %discrimination
    % enables the "Start" button
    set(handles.startButton,'Enable','on')
elseif strcmpi(stim,'LED') && handles.phaseChoice == 3 %discrimination
    warndlg('Cannot do discrimination task with LED')
end
guidata(hObject,handles);

%starts a device with the associated parameters that were set
function startButton_Callback(hObject, eventdata, handles)
guidata(hObject,handles);

global devicesFolder stimulus phaseChoice
% disables the drop down menu to select new devices so that new parameters
% do not get saved until after the most recent device has already started
% runing
set(handles.piSelection,'Enable','off')
pause(.1)

% disables the "Speaker Calibration, "Test Water","Test Sound", and
% "Previous Params" buttons because the device selected is now in use,
% these will not be reeneabled until a new device is selected
set(handles.stimulus,'Enable','off');
set(handles.speakerCalibration,'Enable','off');
set(handles.waterTest,'Enable','off');
set(handles.testLick,'Enable','off');
set(handles.testStim,'Enable','off');
set(handles.previousParams,'Enable','off');

% disables the "Start" button so it can't be used again until all new
% parameters are set
set(handles.startButton,'Enable','off');

% disables the "Set Params" button so it can't be used again until all new
% parameters are set
set(handles.setParams,'Enable','off');

% disables the tone checkboxes so they can't be used again until all new
% parameters are set
set(handles.tone1,'Enable','off');
set(handles.tone2,'Enable','off');
set(handles.tone3,'Enable','off');
set(handles.tone4,'Enable','off');
set(handles.tone5,'Enable','off');
set(handles.tone6,'Enable','off');
set(handles.tone7,'Enable','off');
set(handles.tone8,'Enable','off');
set(handles.tone9,'Enable','off');
set(handles.tone10,'Enable','off');
set(handles.toneNT1,'Enable','off');
set(handles.toneNT2,'Enable','off');
set(handles.toneNT3,'Enable','off');
set(handles.toneNT4,'Enable','off');
set(handles.toneNT5,'Enable','off');
set(handles.toneNT6,'Enable','off');
set(handles.toneNT7,'Enable','off');
set(handles.toneNT8,'Enable','off');
set(handles.toneNT9,'Enable','off');
set(handles.toneNT10,'Enable','off');
% resets all check boxes to unchecked
set(handles.tone1, 'Value', 0);
set(handles.tone2, 'Value', 0);
set(handles.tone3, 'Value', 0);
set(handles.tone4, 'Value', 0);
set(handles.tone5, 'Value', 0);
set(handles.tone6, 'Value', 0);
set(handles.tone7, 'Value', 0);
set(handles.tone8, 'Value', 0);
set(handles.tone9, 'Value', 0);
set(handles.tone10, 'Value', 0);
set(handles.toneNT1, 'Value', 0);
set(handles.toneNT2, 'Value', 0);
set(handles.toneNT3, 'Value', 0);
set(handles.toneNT4, 'Value', 0);
set(handles.toneNT5, 'Value', 0);
set(handles.toneNT6, 'Value', 0);
set(handles.toneNT7, 'Value', 0);
set(handles.toneNT8, 'Value', 0);
set(handles.toneNT9, 'Value', 0);
set(handles.toneNT10, 'Value', 0);

%disables options to select tone levels and resets them to unchecked status
set(handles.level1,'Enable','off')
set(handles.level2,'Enable','off')
set(handles.level3,'Enable','off')
set(handles.level4,'Enable','off')
set(handles.level5,'Enable','off')
set(handles.level6,'Enable','off')
set(handles.level7,'Enable','off')
set(handles.level1,'Value',0)
set(handles.level2,'Value',0)
set(handles.level3,'Value',0)
set(handles.level4,'Value',0)
set(handles.level5,'Value',0)
set(handles.level6,'Value',0)
set(handles.level7,'Value',0)

%disables catch trials option
set(handles.catchTrials,'Enable','off')
set(handles.catchTrials,'Value',0)

% resets the string for the phase selection drop down menu to back to the
% first option of "Training Phase:" and then disables the menu until a new
% device is selected
set(handles.phaseSelection, 'Value', 1);
set(handles.phaseSelection,'Enable','off');

% adds the selected/running devices to this vector to be used in displaying
% current devices and checking their status in the DataGraph Gui
handles.onDevices = {char(handles.cageID.String)};

% changes variable name so it can be saved and passed to the batch job as
% an argument
devicesFolder = handles.devicesFolder;

% shows in drop down menu that device is being used
handles.piSelection.String = strcat(handles.piSelection.String, ' = In Use');

% Keeps start button off if no phase has been selected
if handles.phaseChoice == 0
    set(handles.startButton,'Enable','off')
end

%Selected training phase
phaseChoice = handles.phaseChoice;

%Selected stimulus
stimulus = handles.stimulus.String{handles.stimulus.Value};

%Selected cage/IP address
cageID = handles.cageID.String;
PIIP = handles.rpi.DeviceAddress;
save(handles.filename,'cageID','PIIP','-append')

delete(handles.rpi);
handles.rpi=[];

%renames variable so the information can be saved to a file to be checked
%by the DataGraph Gui
onDevices = handles.onDevices;
save([devicesFolder,'currentDevices.mat'],'onDevices')

% allows the variables defined in this function to be used in other functions
guidata(hObject,handles);

%Delete audioPilotGUI interface
delete(handles.figure1);

% in order to test for functionality
function waterTest_Callback(hObject, eventdata, handles)
%if the toggle button is pressed, the raspberry pi is told to turn on the
%solenoid in order to deliver water
if handles.waterTest.Value == 1
    writeDigitalPin(handles.rpi, 22, 1)
end
%if the toggle button is pressed again to stop the water, the raspberry pi
%is told to turn off the solenoid stopping water delivery
if handles.waterTest.Value == 0
    writeDigitalPin(handles.rpi, 22, 0)
end

guidata(hObject,handles);

function previousParams_Callback(hObject, eventdata, handles)
guidata(hObject,handles);
%tries to load previous parameters, of no previous parameters have been
%saved then a message pops up saying as such
pp = 0;
try
    load([handles.devicesFolder, char(handles.cageID.String), '\PreviousPiParams.mat'])
    pp = 1;
catch
    pp = msgbox('Parameters have not previously been set in this file path. Please input the parameters you wish to run.');
end

%if previous parameters were successfully loaded then they are input into
%the gui
if pp == 1
    handles.target = targetChoice;
    handles.nontarget = nontargetChoice;
    handles.phaseChoice = phaseChoice;
    handles.toneLevel = toneLevelChoice;
    %sets the catch trials radio button value to 0 or 1 depending on the
    %previously saved parameters
    set(handles.catchTrials,'Value',catchTrials);
    
    % this shows which phase was previously selected
    set(handles.tone1,'Enable','on')
    set(handles.tone2,'Enable','on')
    set(handles.tone3,'Enable','on')
    set(handles.tone4,'Enable','on')
    set(handles.tone5,'Enable','on')
    set(handles.tone6,'Enable','on')
    set(handles.tone7,'Enable','on')
    set(handles.tone8,'Enable','on')
    set(handles.tone9,'Enable','on')
    set(handles.tone10,'Enable','on')
    
    if handles.phaseChoice == 1 %shaping
        set(handles.phaseSelection, 'Value', 2);
    end
    if handles.phaseChoice == 2 %detection
        set(handles.phaseSelection, 'Value', 3);
        
        %enables option to select tone level
        set(handles.level1,'Enable','on')
        set(handles.level2,'Enable','on')
        set(handles.level3,'Enable','on')
        set(handles.level4,'Enable','on')
        set(handles.level5,'Enable','on')
        set(handles.level6,'Enable','on')
        set(handles.level7,'Enable','on')
        
        %enables catch trials option
        set(handles.catchTrials,'Enable','on')
    end
    
    if handles.phaseChoice == 3 %discrimination
        set(handles.phaseSelection, 'Value', 4);
        
        % Nonarget tone checkboxes are enabled
        set(handles.toneNT1,'Enable','on')
        set(handles.toneNT2,'Enable','on')
        set(handles.toneNT3,'Enable','on')
        set(handles.toneNT4,'Enable','on')
        set(handles.toneNT5,'Enable','on')
        set(handles.toneNT6,'Enable','on')
        set(handles.toneNT7,'Enable','on')
        set(handles.toneNT8,'Enable','on')
        set(handles.toneNT9,'Enable','on')
        set(handles.toneNT10,'Enable','on')
        
        %enables option to select tone level
        set(handles.level1,'Enable','on')
        set(handles.level2,'Enable','on')
        set(handles.level3,'Enable','on')
        set(handles.level4,'Enable','on')
        set(handles.level5,'Enable','on')
        set(handles.level6,'Enable','on')
        set(handles.level7,'Enable','on')
        
        %enables catch trials option
        set(handles.catchTrials,'Enable','on')
    end
    
    %checks the boxes that were checked in the previous parameters that
    %were set and keeps the unchecked boxes unchecked for targets,
    %nontarget tones, and tone levels
    if ismember(1,handles.target)
        set(handles.tone1, 'Value', 1);
    else
        set(handles.tone1,'Value',0);
    end
    if ismember(1.4,handles.target)
        set(handles.tone2, 'Value', 1);
    else
        set(handles.tone2,'Value',0);
    end
    if ismember(2,handles.target)
        set(handles.tone3, 'Value', 1);
    else
        set(handles.tone3,'Value',0);
    end
    if ismember(2.8,handles.target)
        set(handles.tone4, 'Value', 1);
    else
        set(handles.tone4,'Value',0);
    end
    if ismember(4,handles.target)
        set(handles.tone5, 'Value', 1);
    else
        set(handles.tone5,'Value',0);
    end
    if ismember(5.7,handles.target)
        set(handles.tone6, 'Value', 1);
    else
        set(handles.tone6,'Value',0);
    end
    if ismember(8,handles.target)
        set(handles.tone7, 'Value', 1);
    else
        set(handles.tone7,'Value',0);
    end
    if ismember(11.3,handles.target)
        set(handles.tone8, 'Value', 1);
    else
        set(handles.tone8,'Value',0);
    end
    if ismember(16,handles.target)
        set(handles.tone9, 'Value', 1);
    else
        set(handles.tone9,'Value',0);
    end
    if ismember(22.6,handles.target)
        set(handles.tone10, 'Value', 1);
    else
        set(handles.tone10,'Value',0);
    end
    if ismember(1,handles.nontarget)
        set(handles.toneNT1, 'Value', 1);
    else
        set(handles.toneNT1,'Value',0);
    end
    if ismember(1.4,handles.nontarget)
        set(handles.toneNT2, 'Value', 1);
    else
        set(handles.toneNT2,'Value',0);
    end
    if ismember(2,handles.nontarget)
        set(handles.toneNT3, 'Value', 1);
    else
        set(handles.toneNT3,'Value',0);
    end
    if ismember(2.8,handles.nontarget)
        set(handles.toneNT4, 'Value', 1);
    else
        set(handles.toneNT4,'Value',0);
    end
    if ismember(4,handles.nontarget)
        set(handles.toneNT5, 'Value', 1);
    else
        set(handles.toneNT5,'Value',0);
    end
    if ismember(5.7,handles.nontarget)
        set(handles.toneNT6, 'Value', 1);
    else
        set(handles.toneNT6,'Value',0);
    end
    if ismember(8,handles.nontarget)
        set(handles.toneNT7, 'Value', 1);
    else
        set(handles.toneNT7,'Value',0);
    end
    if ismember(11.3,handles.nontarget)
        set(handles.toneNT8, 'Value', 1);
    else
        set(handles.toneNT8,'Value',0);
    end
    if ismember(16,handles.nontarget)
        set(handles.toneNT9, 'Value', 1);
    else
        set(handles.toneNT9,'Value',0);
    end
    if ismember(22.6,handles.nontarget)
        set(handles.toneNT10, 'Value', 1);
    else
        set(handles.toneNT10,'Value',0);
    end
    if ismember(0,handles.toneLevel)
        set(handles.level1,'Value',1)
    else
        set(handles.level1,'Value',0)
    end
    if ismember(-5,handles.toneLevel)
        set(handles.level2,'Value',1)
    else
        set(handles.level2,'Value',0)
    end
    if ismember(-10,handles.toneLevel)
        set(handles.level3,'Value',1)
    else
        set(handles.level3,'Value',0)
    end
    if ismember(-15,handles.toneLevel)
        set(handles.level4,'Value',1)
    else
        set(handles.level4,'Value',0)
    end
    if ismember(-20,handles.toneLevel)
        set(handles.level5,'Value',1)
    else
        set(handles.level5,'Value',0)
    end
    if ismember(-25,handles.toneLevel)
        set(handles.level6,'Value',1)
    else
        set(handles.level6,'Value',0)
    end
    if ismember(-30,handles.toneLevel)
        set(handles.level7,'Value',1)
    else
        set(handles.level7,'Value',0)
    end
    % "Set Params" button is enabled
    set(handles.setParams,'Enable','on');
end

% allows the variables defined in this function to be used in other functions
guidata(hObject,handles);

%***you will need a microphone for this
function speakerCalibration_Callback(hObject, eventdata, handles)
%reassigns these values in order to use them as arguments for the
%SpeakerCalibration function
rpi = handles.rpi;
filepath = [handles.fileLocation.String,'\'];

%asks user for confirmation of speaker calibration
scbox = questdlg('Are you sure you want to calibrate the speaker on this device?','Calibrate Speaker','Yes','No','No');
ultra = 0;
stim = handles.stimulus.String{handles.stimulus.Value};
SpeakerCalibration(rpi,filepath,stim, handles.cageID.String)

guidata(hObject,handles);

% selected device to make sure the speaker is functioning properly
function testStim_Callback(hObject, eventdata, handles)
%while the button is pressed (press once), a sound will play. The file
%is 4.5 seconds so the loop will occur about every 6 seconds
stim = handles.stimulus.String{handles.stimulus.Value};
if strcmpi(stim,'Tone')
    wavFile = 'Tone_2kHz_0dB.wav';
    while handles.testStim.Value == 1
        try
            system(handles.rpi,['sudo aplay -D plughw:1,0 /home/audiopilot/' stim '/' wavFile]);
            disp('sound has played')
        catch
            disp('Sound error')
        end
        pause(1)
    end
else
    %if the toggle button is pressed, the raspberry pi is told to turn on the LED
    if handles.testStim.Value == 1
        writeDigitalPin(handles.rpi, 24, 1)
    end
    %if the toggle button is pressed again, the raspberry pi
    %is told to turn off the LED
    if handles.testStim.Value == 0
        writeDigitalPin(handles.rpi, 24, 0)
    end
end

guidata(hObject,handles);

function testLick_Callback(hObject, eventdata, handles)
lickPin = 1;
set(handles.testLick,'BackgroundColor',[.94 .94 .94])
while handles.testLick.Value == 1
    try
        lickPin = readDigitalPin(handles.rpi, 5);
        drawnow;
    catch
        disp('Digital read error')
        pause(5)
    end
    if lickPin == 0
        set(handles.testLick,'BackgroundColor',[0 .94 0])
    else
        set(handles.testLick,'BackgroundColor',[.94 .94 .94])
    end
end
set(handles.testLick,'BackgroundColor',[.94 .94 .94])

function stimulus_Callback(hObject, eventdata, handles)
stim = handles.stimulus.String{handles.stimulus.Value};
if strcmpi(stim,'Tone')
    F=1000.*2.^[0:.5:4.5];
    for i = 1:length(F)
        eval(['set(handles.tone' num2str(i) ',''String'',''Tone ' num2str(round(F(i)/1000,1)) ''')'])
        eval(['set(handles.toneNT' num2str(i) ',''String'',''Tone ' num2str(round(F(i)/1000,1)) ''')'])
    end
else
    
    %tone levels disabled
    set(handles.level1,'Enable','off')
    set(handles.level2,'Enable','off')
    set(handles.level3,'Enable','off')
    set(handles.level4,'Enable','off')
    set(handles.level5,'Enable','off')
    set(handles.level6,'Enable','off')
    set(handles.level7,'Enable','off')
    
    %Tone checkboxes are disabled
    set(handles.tone1,'Enable','off');
    set(handles.tone2,'Enable','off');
    set(handles.tone3,'Enable','off');
    set(handles.tone4,'Enable','off');
    set(handles.tone5,'Enable','off');
    set(handles.tone6,'Enable','off');
    set(handles.tone7,'Enable','off');
    set(handles.tone8,'Enable','off');
    set(handles.tone9,'Enable','off');
    set(handles.tone10,'Enable','off');
    set(handles.toneNT1,'Enable','off');
    set(handles.toneNT2,'Enable','off');
    set(handles.toneNT3,'Enable','off');
    set(handles.toneNT4,'Enable','off');
    set(handles.toneNT5,'Enable','off');
    set(handles.toneNT6,'Enable','off');
    set(handles.toneNT7,'Enable','off');
    set(handles.toneNT8,'Enable','off');
    set(handles.toneNT9,'Enable','off');
    set(handles.toneNT10,'Enable','off');
    
end

function stimulus_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startDelay_Callback(hObject, eventdata, handles)
function startDelay_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function trialBlockDur_Callback(hObject, eventdata, handles)
function trialBlockDur_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function cageID_Callback(hObject, eventdata, handles)
function cageID_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

