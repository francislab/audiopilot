function out = VolumeConversion(in,direction,Microphone)
% VolumeConversion converts between SPL [dB] and Voltage

switch Microphone
    case 'BK4138'
        dBSPL0 = 94;
        V0 = 0.014; %V/Pa; Value aquired using micCalibration.m
    otherwise
        error('Microphone not tested yet.');
        
end

if strcmp(direction,'dB2V')
    out = V0*10.^((in-dBSPL0)/20);
    
elseif strcmp(direction,'V2dB')
    out = dBSPL0 + 20*log10(in/V0);
    
else
    error('Not a valid conversion!');
    
end