function audioPilotBehavior

global quit intervalStop pauseProgram blockInterval trialRunning paused devicesFolder stimulus phaseChoice
% sets variables for certain loop stoppages to zero, quit changes to 1 when
% the Stop buton on the audiopilotGui is pressed and it stops the entire
% phase, intervalStop does the same as quit except it can be accessed
% during an inter-block interval, pauseProgram becomes 1 when all the
% devices are paused through the audiopilotGui.
quit = 0;
intervalStop = 0;
pauseProgram = 0;
paused=0;

% loads parameters that were set in the audiopilotGui
load([devicesFolder 'PiParams.mat'])

%Load speaker calibration values
load([devicesFolder 'SpeakerCalibration_' cageID '.mat'])

% makes sure that a folder exists for the device that is running, if a
% folder doesn't exist then one is created
checkFolder = exist([devicesFolder,cageID]);
if checkFolder == 0
    mkdir(devicesFolder,cageID)
end
movefile([devicesFolder 'PiParams.mat'],[devicesFolder, cageID '/PiParams.mat'])

% checks to see if a general performance (data) file already exists in the
% folder that is associated with the device. If the file already exists,
% then that file is renamed with a message that indicates there was an
% error. This is a failsafe, the only way this file would exist at the
% beginning of the function is if the function had stopped unexpectedly or
% erroneously. Otherwise, this file is deleted at the end of the function
% and the data is saved to a separate file that is time stamped.
checkFile = exist([devicesFolder, cageID '/performance.mat']);
if checkFile == 2
    try
        load([devicesFolder, cageID '/performance.mat'])
        movefile ([devicesFolder, cageID '/performance.mat'], [devicesFolder, cageID '/performance' datestr(now,'dd-mm-yyyy_HH.MM') '_stop_error.mat'])
        
        %if the performance file exists but matlab can't load it, i.e. it
        %is corrupted, the backup file will be renamed and saved with an
        %error message
    catch
        load([devicesFolder,cageID '/performance_backup.mat'])
        movefile([devicesFolder,cageID '/performance_backup.mat'],[devicesFolder, cageID '/performance' datestr(now,'dd-mm-yyyy_HH.MM') '_stop_error.mat'])
    end
end

%variable to determine whether the phase is running trials or on an
%inter-block interval
blockInterval = 0;

%these variables indicate which phase is running, used for data analysis
if phaseChoice == 1
    phaseName = 'Shaping';
elseif phaseChoice == 2
    phaseName = 'Detection';
elseif phaseChoice ==3
    phaseName = 'Discrimination';
end
phasechoice = phaseChoice;

%records of the number of each responses, respectively hit, early, miss,
%and false alarm
hitCount = 0;
earlyCount = 0;
missCount = 0;
falseAlarmCount = 0;
correctRejectionCount = 0;
catchCount = 0;

%records of the number of trials
totalTrials = 0;

%records of the time each trial starts
timeStamp = {};

%Compiles the licks from each trial into a matrix, each row is another trial of recorded licks
lickTotal = {};
lickTotalTarget = {};
lickTotalNonTarget = {};

%variable used to find when the first lick occurred
lickData = [];

% nbins can be adjusted based depending on if the sampling rate of the
% sensor is too fast or too slow, this value was picked based on observing
% how fast a mouse can lick and react to the stimulus, the bins are used to
% break up the lick response data
nbins = 75;

% Compiles all the separate lick histograms into one big data set
totalData = [];
totalDataNonTarget=[];
totalDataTarget=[];

%sets the x axis based on the nbins variable for plotting histograms later in data analysis
xaxis = linspace(0,4,nbins);

% records the first lick for all trials
lickResponse = zeros(1,nbins);
lickResponseTarget = zeros(1,nbins);
lickResponseNonTarget = zeros(1,nbins);

% saves tone selections from gui to this function
target = targetChoice;
nontarget = nontargetChoice;

% saves tone level selections
toneLevel = toneLevelChoice;
if isempty(toneLevel)
    toneLevel = 0;
end

%records which tone played during that trial
toneVec = [];

%records whether the response was a hit 'H', early 'E', a miss 'M', or a false alarm 'F'
responseVec = [];

%records which level the tone was played at
levelVec = [];

%Target or Non-Target vector
targetNontargetVec = [];

% saves all these variables to a performance file to be saved at the end of
% each loop. this file is used for data analysis in the DataGraph GUI
save([devicesFolder, cageID '/performance.mat'],'phaseName','hitCount','catchCount','missCount','earlyCount',...
    'falseAlarmCount','correctRejectionCount','xaxis','totalTrials','lickResponse','lickResponseTarget',...
    'lickResponseNonTarget','totalData','totalDataTarget','totalDataNonTarget','target',...
    'nontarget','phasechoice','cageID','timeStamp','blockInterval','toneVec',...
    'levelVec','responseVec','targetNontargetVec')

%starts a timer that will indicate the end of a block and the beginning of an inter-block interval
ibi = tic;

%Start delay
if startDelay > 0
    disp([num2str(startDelay) ' minute start delay'])
    pause(startDelay*60)
end

%Connect to Raspberry Pi
try
    rpi = raspi(PIIP,'audiopilot','thirstymouse');
    system(rpi,'hostname');
catch
    disp('Cannot connect to Pi. Restart Matlab and Pi.')
    error = 1;
    quit = 1;
end

%quit becomes 1 when the Stop button is pressed on the audiopilotGui
while quit == 0
    
    %if an error occurs, then the value will become 1 and the data for that trial will be voided.
    error = 0;
    
    %records the number of trials
    totalTrials = totalTrials + 1;
    
    %this records the time of each trial
    timeStamp = [timeStamp; {totalTrials datestr(now,'mm/dd/yyyy HH:MM:SS')}];
    
    save([devicesFolder, cageID '/performance.mat'],'phaseName','hitCount','catchCount','missCount','earlyCount',...
        'falseAlarmCount','correctRejectionCount','xaxis','totalTrials','lickResponse','lickResponseTarget',...
        'lickResponseNonTarget','totalData','totalDataTarget','totalDataNonTarget','target',...
        'nontarget','phasechoice','cageID','timeStamp','blockInterval','toneVec',...
        'levelVec','responseVec','targetNontargetVec')
    
    %if the trial results in the corresponding response, the variable will equal 1
    hit = 0;
    early = 0;
    falseAlarm = 0;
    correctrejection = 0;
    
    %records the licks of the current trial, then resets each loop
    lickTrial = [];
    
    % Compiles the licks from each trial into a matrix, each row is another trial of recorded licks
    lickTotal{totalTrials} = [];
    
    % Compiles all the separate lick histograms into one big data set
    totalData(totalTrials,1:nbins) = nan;
    totalDataTarget(totalTrials,1:nbins) = nan;
    totalDataNonTarget(totalTrials,1:nbins) = nan;
    
    %records which tone played during that trial
    toneVec{totalTrials} = [];
    
    %records whether the response was a hit 'H', early 'E', a miss 'M', or a false alarm 'F'
    responseVec{totalTrials} = 'X';
    
    %records which level the tone was played at
    levelVec{totalTrials} = [];
    
    %if the catch trials option is selected in the audiopilotGui, then sielnt
    %trials will occur in 30 percent of the trials
    percent = 10;
    rng('shuffle')
    if catchTrials == 1
        randomNumber = randi(100);
    else
        randomNumber = 100;
    end
    
    % this delay (only after shaping phase) ensures that a new trial
    % doesn't start until that the animal hasn't licked for 5 consecutive
    % seconds
    C = tic;
    lickPin = 0;
    while toc(C) < 5 && intervalStop == 0
        try
            lickPin = readDigitalPin(rpi, 5);
            drawnow;
        catch
            disp('Digital read error')
            pause(5)
        end
        if lickPin == 0
            C = tic;
        end
    end
    
    %starts a timer to help align the sound file with the trial time
    V = tic;
    
    %30 percent of trials randomNumber will be smaller than percent so if
    %silent trials were selected then on 30 percent of trials there will be
    %no sound
    noWater=0;
    TorNT = 'T';
    targetNontargetVec{totalTrials} = TorNT;
    rng('shuffle')
    if totalTrials == 1 || (randomNumber > percent)
        if strcmpi(stimulus,'Tone')
            % sound files are stored as Tone_XkHz_YdB.wav, and the
            % different values of X are the different tone frequencies. The
            % sound variable selects a random value from the specific tones
            % that were selected in the audiopilotGui. The different values of
            % Y are different tone levels and the randLevel variable
            % selects a random value from the specific tone levels what
            % were selected in the audiopilotGui
            sound = num2str(target(randi(length(target))));
            randLevel = num2str(toneLevel(randi(length(toneLevel))));
            
            %Discrimination phase: nontarget on 50% of trials
            if phaseChoice == 3 && rand >.5
                TorNT = 'NT';
                targetNontargetVec{totalTrials} = TorNT;
                sound = num2str(nontarget(randi(length(nontarget))));
                
            end
            
            % command to raspberry pi to play random sound selection
            system(rpi,['sudo amixer set Speaker ' num2str(R.VolPercent) '%']);
            system(rpi,['sudo aplay -D plughw:1,0 /home/audiopilot/Tone/Tone_' sound 'kHz_' randLevel 'dB.wav </dev/null >/dev/null 2>&1 &']);
        else
            sound = target(1);
            randLevel = 'X';
            
        end
    else
        %this trial will have no stimulus
        disp('catch trial')
        sound = '0';
        randLevel = 'X';
        noWater=1;
    end
    
    %records which sound and which tone level were played, sound = 0 and
    %randLevel = X for silent trials
    toneVec{totalTrials} = sound;
    levelVec{totalTrials} = randLevel;
    
    % ensures that the sound begins at 1 second in the trial, there is a
    % delay between calling the command and executing the command
    while toc(V) < .3
        A = 0;
    end
    
    % timer for the beginning of each trial
    A = tic;
    
    %reset if stim or water has been presented
    stimOn=0;
    soleniodOn=0;
    trialRunning=0;
    response=0;
    
    % time of a trial is 4 seconds
    while toc(A) < 4
        trialRunning = 1;
        
        try
            % tells the raspberry pi to read the capacitive touch sensor
            lickPin = readDigitalPin(rpi, 5);
            drawnow;
            
            % if a lick is detected before the response window (<1s) then the
            % response is considered "early", punishment for this response is a
            % longer delay time in between trials, delay is set to 20 seconds
            if toc(A) < 1.0 && lickPin == 0
                early = 1;
            end
            
            % lickTrial will be timestamped every time the capacitive touch
            % sensor is triggered, this is how licks are recorded each trial
            if lickPin == 0
                lickTrial = [lickTrial; toc(A)];
            end
            
            %Show LED stimulus if selected
            if strcmpi(stimulus,'LED') && stimOn==0 && toc(A)>1
                %Flash LED for 1 second
                writeDigitalPin(rpi, 24, 1);
                t2=timer;
                t2.StartDelay=1;
                t2.TimerFcn = @(~,~) writeDigitalPin(rpi, 24, 0);
                start(t2)
                stimOn=1;
                
            end
            
            if phaseChoice == 1
                if toc(A) >= 1.25
                    %Shaping water
                    if  soleniodOn==0 && noWater==0
                        %Trigger solenoid for 2 seconds
                        writeDigitalPin(rpi, 22, 1);
                        t2=timer;
                        t2.StartDelay=2;
                        t2.TimerFcn = @(~,~) writeDigitalPin(rpi, 22, 0);
                        start(t2)
                        disp('solenoid on')
                        soleniodOn=1;
                    end
                end
                
                if lickPin == 0
                    response = 1;
                    %Deliver water
                    if  soleniodOn==0
                        %Trigger solenoid for 2 seconds
                        writeDigitalPin(rpi, 22, 1);
                        t2=timer;
                        t2.StartDelay=2;
                        t2.TimerFcn = @(~,~) writeDigitalPin(rpi, 22, 0);
                        start(t2)
                        disp('solenoid on')
                        soleniodOn=1;
                    end
                end
                
            elseif toc(A) >= 1.0 && toc(A) <4.0 && lickPin == 0 && early == 0 && length(lickTrial) == 1
                response = 1;
                %if there is a response in this window during a catch trial, the response is considered a "false alarm"
                if sound == '0'
                    catchCount = 1;
                    
                else
                    if TorNT == 'T'
                        %Deliver water
                        if  soleniodOn==0
                            %Trigger solenoid for 2 seconds
                            writeDigitalPin(rpi, 22, 1);
                            t2=timer;
                            t2.StartDelay=2;
                            t2.TimerFcn = @(~,~) writeDigitalPin(rpi, 22, 0);
                            start(t2)
                            disp('solenoid on')
                            soleniodOn=1;
                        end
                    end
                end
            end
        catch
            disp('Digital read/write error: Trial voided')
            error = 1;
            break
        end
    end
    trialRunning = 0;
    
    %creates a backup file for the data
    copyfile([devicesFolder, cageID '/performance.mat'],[devicesFolder, cageID '/performance_backup.mat'])
    
    if error == 0
        % these count variables are used for a histogram to show
        % quantitatively how the mouse responded. the responseVec vector
        % also saves the type of response with a corresponding letter
        % delays in between trials after hit and miss responses are between
        % 5 and 9 seconds, early  and false alarm trials result in 20
        % second delays.
        rng('shuffle')
        if early == 1
            disp('early')
            if phaseChoice ~= 1
                delay = 20 + randi([5,9]);
                pause(delay)
            end
            earlyCount = earlyCount + 1;
            responseVec{totalTrials} = 'E';
        else
            if response == 0 && strcmpi(TorNT,'T')
                disp('miss')
                delay = randi([5,9]);
                pause(delay)
                missCount = missCount + 1;
                responseVec{totalTrials} = 'M';
            end
            if response == 1 && strcmpi(TorNT,'T')
                disp('hit')
                delay = randi([5,9]);
                pause(delay)
                hitCount = hitCount + 1;
                responseVec{totalTrials} = 'H';
            end
            
            if response == 1 && strcmpi(TorNT,'NT')
                disp('false alarm')
                delay = 20 + randi([5,9]);
                pause(delay)
                falseAlarmCount = falseAlarmCount + 1;
                responseVec{totalTrials} = 'F';
            end
            if response == 0 && strcmpi(TorNT,'NT')
                disp('correct rejection')
                delay = randi([5,9]);
                pause(delay)
                correctRejectionCount = correctRejectionCount + 1;
                responseVec{totalTrials} = 'C';
            end
        end
    end
    
    % Compiles the licks from each trial into a matrix, each row is another trial of recorded licks
    lickTotal{totalTrials} = lickTrial;
    
    % The next three lines find where the first lick occurred in the most recent trial
    lickData = cell2mat(lickTotal(totalTrials));
    
    % This line specifically separates the recorded licks into a histogram
    [lickHistogram,edges] = histcounts(lickData,nbins,'BinLimit',[0 4]);
    firstLick = find(lickHistogram>0,1);
    
    % This logs the first lick into a histogram of all the first licks for all trials
    lickResponse(1,firstLick) = lickResponse(1,firstLick) + 1;
    
    % Compiles all the separate lick histograms into one big data set
    totalData(totalTrials,1:nbins) = lickHistogram;
    
    %Now by target vs nontarget
    if TorNT == 'T'
        lickResponseTarget(1,firstLick) = lickResponseTarget(1,firstLick) + 1;
        totalDataTarget(totalTrials,1:nbins) = lickHistogram;
        
    else
        lickResponseNonTarget(1,firstLick) = lickResponseNonTarget(1,firstLick) + 1;
        totalDataNonTarget(totalTrials,1:nbins) = lickHistogram;
        
    end
    
    save([devicesFolder, cageID '/performance.mat'],'phaseName','hitCount','catchCount','missCount','earlyCount',...
        'falseAlarmCount','correctRejectionCount','xaxis','totalTrials','lickResponse','lickResponseTarget',...
        'lickResponseNonTarget','totalData','totalDataTarget','totalDataNonTarget','target',...
        'nontarget','phasechoice','cageID','timeStamp','blockInterval','toneVec',...
        'levelVec','responseVec','targetNontargetVec')
    
    %Plot data
    plotAudioPilotBehavior
    
    % inter-block interval, after 60 minutes of trials there is a 60 minute break
    if toc(ibi) > trialBlockDur*60
        %keeps track of loops/time
        trialTime = 0;
        %saves a variable so that the DataGraph GUI knows that the device
        %is on a break
        blockInterval = 1;
        save([devicesFolder, cageID  '/performance.mat'],'blockInterval','-append')
        % loop runs 3600 times unless stop button on audiopilotGui is pressed,
        % if stop button is pressed, interval loop stops and main loop also
        % stops as the varialbes intervalStop and quit are both changed to
        % equal 1
        while trialTime < trialBlockInterval*60 && intervalStop == 0
            trialTime = trialTime + 1;
            pause(1)
        end
        %restarts the inter-block interval timer
        ibi = tic;
        %resets this variable so the status of the device is no longer on a
        %break
        blockInterval = 0;
        save([devicesFolder, cageID  '/performance.mat'],'blockInterval','-append')
    end
    
    %Pause
    while paused
        drawnow;
    end
    
end

% the final data will be saved to a time stamped performance file
save([devicesFolder, cageID '/performance' datestr(now,'dd-mm-yyyy_HH.MM') '.mat'],'phaseName','hitCount','catchCount','missCount','earlyCount',...
    'falseAlarmCount','correctRejectionCount','xaxis','totalTrials','lickResponse','lickResponseTarget',...
    'lickResponseNonTarget','totalData','totalDataTarget','totalDataNonTarget','target',...
    'nontarget','phasechoice','cageID','timeStamp','blockInterval','toneVec',...
    'levelVec','responseVec','targetNontargetVec')

% deletes the generic performance file
delete([devicesFolder, cageID '/performance.mat'])

%clear connection to Raspberry Pi
warning('off','shared_linuxservices:nanoreqclient:receiveTimeout')
clearRPI=0;
while clearRPI==0
    try
        system(rpi,'sudo reboot');
    catch
        clear rpi
        clearRPI=1;
    end
end