function SpeakerCalibration(rpi,filepath, stimulus, cageID)

global globalparams Response

%%%% Initialize variables and hardware
globalparams.speaker='Pioneer';
globalparams.microphone='BK4138';
globalparams.VRef=1;
globalparams.dBSPLRef=60;

%Duration of calibration sounds
globalparams.LStim=10;

%high-pass corner frequency for recordings
globalparams.highcut = 23000;
globalparams.SR=48000;
globalparams.Fband=[2000 23000];

globalparams.R=[];

%%%% Record a broad-noise that is set to +/- the maximum output voltage (VRef)
globalparams.NSteps = round(globalparams.LStim*globalparams.SR);
Noise = wgn(1,globalparams.NSteps,1);
Noise = Noise(:);
ramp = hanning(round(.2 * globalparams.SR*2));
ramp = ramp(1:floor(length(ramp)/2));
Noise(1:length(ramp)) = Noise(1:length(ramp)) .* ramp;
Noise(end-length(ramp)+1:end) = Noise(end-length(ramp)+1:end) .* flipud(ramp);
Noise = [zeros(globalparams.SR*2,1); Noise./max(abs(Noise)); zeros(globalparams.SR*2,1)];
audiowrite([filepath,'noise.wav'],Noise,globalparams.SR)
putFile(rpi,[filepath,'noise.wav'],'/home/audiopilot')
system(rpi, ['sudo amixer set ''Auto Gain Control'' off']);
system(rpi,'sudo amixer set Speaker 75%');
system(rpi,'sudo amixer set Mic 100%');
system(rpi,['sudo aplay -D plughw:1,0 /home/audiopilot/noise.wav | sudo arecord -D plughw:1,0 -d 15 -f dat -r ' num2str(globalparams.SR) ' -c 1 /home/audiopilot/calnoise.wav']);
getFile(rpi,'/home/audiopilot/calnoise.wav',filepath);
system(rpi,'sudo rm /home/audiopilot/calnoise.wav | sudo rm /home/audiopilot/noise.wav');
AllData = audioread([filepath,'calnoise.wav']);
Response = AllData(globalparams.SR*3:end-(globalparams.SR*3));
Response(1:length(ramp)) = Response(1:length(ramp)) .* ramp;
Response(end-length(ramp)+1:end) = Response(end-length(ramp)+1:end) .* flipud(ramp);
fprintf(['\n ====== Done Recording ======\n']);
scrsz = get(0,'ScreenSize');
globalparams.Fig=figure('position',[scrsz(3)/4 scrsz(4)*.25 scrsz(3)/2 scrsz(4)/2]);
set(gcf,'Name',['Speaker: ',globalparams.speaker,' (SR=',num2str(globalparams.SR),'Hz)'],'MenuBar','none','Toolbar','figure');

%%%% Estimate the whitening filter
%This program uses the recorded response to a noise played through a speaker to estimate (1) the transfer function from the speaker to the
%microphone and (2) the "Inverse" transfer function, which is the equalizing spectrum that flatens the spectral output of the speaker. The
%inverse transfer function is found by divding an idealized flat spectrum by the forward transfer function.
R = globalparams.R;
R.Fs = globalparams.SR;
ResponseSpec=(2/length(Response))*(abs(fft(Response)));
ResponseSpecdB=VolumeConversion(ResponseSpec,'V2dB',globalparams.microphone);
ResponseSpecdB=smooth(ResponseSpecdB,2^10);

%Plot spectrum
figure(globalparams.Fig)
subplot(2,2,1:2)
cla
f = linspace(0,R.Fs,length(ResponseSpecdB));
plot(f/1000,ResponseSpecdB,'b','linewidth',2);
set(gca,'Xscale','log')
hold on;
grid on
xlim(globalparams.Fband./1000)
aa=axis;
ylim([aa(3)-1 aa(4)+1]);
ylabel('dB SPL')
xlabel('Frequency (kHz)');
title('Recorded Noise Spectrum Level','fontsize',10);
set(gca,'xtick',[5    10    20])
set(gca,'xticklabel',[5    10    20])
R.ResponseSpecdB=ResponseSpecdB;
globalparams.R = R;
drawnow

%Estimate whitening filter
fidx = find(f>=globalparams.Fband(1) & f<=globalparams.Fband(2));
WhiteSpecdB = repmat(-100,size(ResponseSpecdB));
WhiteSpecdB(fidx) = min(ResponseSpecdB(fidx))-ResponseSpecdB(fidx);
globalparams.R.WhiteningSpec = VolumeConversion(WhiteSpecdB','dB2V',globalparams.microphone);

%%%% Estimate amplifier gain
WhiteningSpec=globalparams.R.WhiteningSpec';
R = globalparams.R;
Noise = wgn(1,length(WhiteningSpec),1);
Noise = Noise(:);
ramp = hanning(round(.2 * globalparams.SR*2));
ramp = ramp(1:floor(length(ramp)/2));
Noise(1:length(ramp)) = Noise(1:length(ramp)) .* ramp;
Noise(end-length(ramp)+1:end) = Noise(end-length(ramp)+1:end) .* flipud(ramp);
CalbNoise = Noise./max(abs(Noise));
[b a] = butter(3,globalparams.Fband./(globalparams.SR/2));
CalbNoise = filtfilt(b,a,CalbNoise);
CalbNoise = CalbNoise./max(abs(CalbNoise));

%Whiten the noise for gain calibration
CalbNoiseWhite = IOCalibrationFilter(CalbNoise, WhiteningSpec, globalparams.microphone);
ramp = hanning(round(.2 * globalparams.SR*2));
ramp=ramp(1:floor(length(ramp)/2));
CalbNoiseWhite = CalbNoiseWhite(globalparams.SR*3:end-(globalparams.SR*3));
CalbNoiseWhite(1:length(ramp))=CalbNoiseWhite(1:length(ramp)) .* ramp;
CalbNoiseWhite(end-length(ramp)+1:end)=CalbNoiseWhite(end-length(ramp)+1:end) .* flipud(ramp);
CalbNoiseWhite = [zeros(globalparams.SR*2,1); CalbNoiseWhite./max(abs(CalbNoiseWhite)); zeros(globalparams.SR*2,1)];
audiowrite([filepath,'CalbNoiseWhite.wav'],CalbNoiseWhite,globalparams.SR)
putFile(rpi,[filepath,'CalbNoiseWhite.wav'],'/home/audiopilot')

%Play noise and adjust amplifier gain
CurrentdB=0;
VolPercent=100;
system(rpi,['sudo amixer set Speaker ' num2str(VolPercent) '%']);
while abs(globalparams.dBSPLRef-CurrentdB) > 3
    system(rpi,['sudo arecord -D plughw:1,0 -d 7 -f dat -r ' num2str(globalparams.SR) ' -c 1 CalbNoiseWhiteGain.wav | sudo aplay -D plughw:1,0 /home/audiopilot/CalbNoiseWhite.wav']);
    getFile(rpi,'/home/audiopilot/CalbNoiseWhiteGain.wav',filepath);
    system(rpi,'sudo rm /home/audiopilot/CalbNoiseWhiteGain.wav');
    AIdata = audioread([filepath,'CalbNoiseWhiteGain.wav']);
    AIdata = AIdata(globalparams.SR*3:end-globalparams.SR*3);
    AIdata(1:length(ramp))=AIdata(1:length(ramp)) .* ramp;
    AIdata(end-length(ramp)+1:end)=AIdata(end-length(ramp)+1:end) .* flipud(ramp);
    [b a] = butter(3,[globalparams.Fband(1) globalparams.Fband(2)]./(globalparams.SR/2),'bandpass');
    AIdata = filtfilt(b,a,AIdata);
    Vcurrent = rms(AIdata);
    CurrentdB = VolumeConversion(Vcurrent,'V2dB',globalparams.microphone);
    disp(['dB: ' num2str(CurrentdB)])
    disp(['dB Goal Difference: ' num2str(globalparams.dBSPLRef-CurrentdB)])
    disp('')
    if (globalparams.dBSPLRef-CurrentdB)<0
        VolPercent = VolPercent-5;
    else
        VolPercent = VolPercent+5;
    end
    system(rpi,['sudo amixer set Speaker ' num2str(VolPercent) '%']);
    pause(2)
end
R.cdBSPL = VolumeConversion(Vcurrent,'V2dB',globalparams.microphone);
fprintf(['\n=> Level: ',num2str(R.cdBSPL),'\n']);
R.Fs=globalparams.SR;
R.VolPercent = VolPercent;
globalparams.R = R;

%Test calibration
globalparams.NSteps = round(globalparams.LStim*globalparams.SR);
F=globalparams.Fband(1).*2.^[0:.5:5.5];
F=F(F<globalparams.Fband(2));
tonedur=1;
t=0:1/globalparams.SR:(tonedur)-(1/globalparams.SR);
fs=globalparams.SR;
t0 = 0:1/fs:1-(1/fs);
t1=t0;
t2 = 0:1/fs:2-(1/fs);
AllWhiteStims=[];
M=[];
Stims=[];
for i = 1:length(F)
    Trial =[];
    stim = sin(2*pi*F(i).*t)';
    stim(1:length(ramp)) = stim(1:length(ramp)) .* ramp;
    stim(end-length(ramp)+1:end) = stim(end-length(ramp)+1:end) .* flipud(ramp);
    stim = stim./max(abs(stim));
    ramp = hanning(round(.02 * globalparams.SR*2));
    ramp = ramp(1:floor(length(ramp)/2));
    stim(1:length(ramp)) = stim(1:length(ramp)) .* ramp;
    stim(end-length(ramp)+1:end) = stim(end-length(ramp)+1:end).*flipud(ramp);
    
    %Whiten the stim
    spec = globalparams.R.WhiteningSpec';
    mic = globalparams.microphone;
    stimWhite = IOCalibrationFilter(stim, spec, mic);
    stimWhite(1:length(ramp)) = stimWhite(1:length(ramp)) .* ramp;
    stimWhite(end-length(ramp)+1:end) = stimWhite(end-length(ramp)+1:end) .* flipud(ramp);
    Trial = [zeros(length(t0),1); stimWhite; zeros(length(t2),1)];
    AllWhiteStims{i} = Trial;
    M(i) = max(abs(Trial));
    Stims = [Stims; zeros(length(t0)/2,1); stimWhite; zeros(length(t0)/2,1); ];
end
Stims = [zeros(globalparams.SR*2,1); Stims./max(abs(Stims))];
L = ceil(length(Stims)./globalparams.SR);

%Play tones
system(rpi,['sudo amixer set Speaker ' num2str(R.VolPercent) '%']);
audiowrite([filepath,'Stims.wav'],Stims,globalparams.SR)
putFile(rpi,[filepath,'Stims.wav'],'/home/audiopilot')
fprintf(['\n ====== Play Calibration Stimuli ====== \n']);
system(rpi,['sudo arecord -D plughw:1,0 -d ' num2str(L) ' -f dat -r ' num2str(globalparams.SR) ' -c 1 StimsCalb.wav | sudo aplay -D plughw:1,0 /home/audiopilot/Stims.wav']);
getFile(rpi,'/home/audiopilot/StimsCalb.wav',filepath);
system(rpi,'sudo rm /home/audiopilot/StimsCalb.wav | sudo rm /home/audiopilot/Stims.wav');
AIdata = audioread([filepath,'StimsCalb.wav']);
AIdata = AIdata(globalparams.SR*2:end);
f1 = globalparams.Fband(1)-500;
f2 = globalparams.Fband(2)+500;
[b a] = butter(6,[f1 f2]./(globalparams.SR/2));
AIdata = filtfilt(b,a,AIdata);
AIdata(1:length(ramp))=AIdata(1:length(ramp)) .* ramp;
AIdata(end-length(ramp)+1:end)=AIdata(end-length(ramp)+1:end) .* flipud(ramp);
figure(globalparams.Fig)
subplot(2,2,3)
t=0:1/globalparams.SR:(length(AIdata)/R.Fs)-(1/globalparams.SR);
plot(t,AIdata,'k')
xlabel('Time (s)')
ylabel('Volts')
title([{'Calibrated Test Stim Waveform'}],'fontsize',10)
aa=axis;
axis tight
ylim([-max(aa(3:end)) max(aa(3:end))].*1.5)
grid on
f=linspace(0,R.Fs,length(AIdata));
subplot(2,2,4)
AIDataSpec=(2/length(AIdata))*(abs(fft(AIdata)));
AIDataSpecdB=VolumeConversion(AIDataSpec,'V2dB',globalparams.microphone);
plot(f/1000,AIDataSpecdB,'k')
xlim([(-500+globalparams.Fband(1))/1000 (500+globalparams.Fband(2))/1000])
xlabel('Frequency (kHz)')
ylabel('dB SPL')
title('Calibrated Test Stim Spectrum','fontsize',10)
aa=axis;
ylim([0 aa(4)])
grid on
globalparams.R = R;

pause(1)

%Save calibration
FileName = ['SpeakerCalibration_' cageID '.mat'];
globalparams.R.VRef = globalparams.VRef;
globalparams.R.dBSPLRef = globalparams.dBSPLRef;
R = globalparams.R;
fprintf(['\n ====== Saving Calibration ======\n']);
save([filepath, '\Devices\' FileName],'R')

%Save calibrated stimulus
try
    system(rpi,['sudo rm /home/audiopilot/' stimulus '/*.wav']);
end
try
    system(rpi,['sudo mkdir -p -m 777 /home/audiopilot/' stimulus]);
end

%Level attentuations & saving stimuli
saved=0;
h=waitbar(0,'Saving tones on Pi. Please wait...');
LL = -30:5:0;
s=0;
for i = 1:length(F)
    for ii = 1:length(LL)
        AttendB=10^(LL(ii)/20);
        Trial = AttendB*(AllWhiteStims{i}./max(M));
        fname = [stimulus '_' num2str(round(F(i)./1000,1)) 'kHz_' num2str(LL(ii)) 'dB.wav'];
        audiowrite([filepath, fname],Trial,globalparams.SR)
        putFile(rpi,[filepath, fname],['/home/audiopilot/' stimulus]);
        delete([filepath, fname]);
        s = s+1;
        waitbar(s/15,h)
    end
end
waitbar(1,h,'Done saving!')
pause(2)
close(h)


fprintf(['\n ====== Done! ======\n']);


