function varargout = plotAudioPilotBehavior(varargin)

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @plotAudioPilotBehavior_OpeningFcn, ...
    'gui_OutputFcn',  @plotAudioPilotBehavior_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before plotAudioPilotBehavior is made visible.
function plotAudioPilotBehavior_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;

handles.performancefile=[];
if ~isempty(varargin)
    handles.performancefile = varargin{1}{1};
end

%sets the defaults for all the buttons and text boxes
set(handles.checkDevices,'Enable','off')
set(handles.waitStatus,'visible','off')
set(handles.runStatus,'visible','off')
set(handles.blockStatus,'visible','off')
set(handles.failStatus,'visible','off')
set(handles.pauseStatus,'visible','off')

%sets file path to the current file that Matlab has open
handles.fileLocation.String = pwd;

% checks to see if a Devices folder already exists in this location
checkPath2 = exist([handles.fileLocation.String '\Devices']);

% if the folder doesn't exist then a dialogue box opens and asks user if
% this is the correct file path
if checkPath2 == 0
    fp = msgbox('No data can be found for the device. Change file path?');
else
    handles.devicesFolder = [handles.fileLocation.String '\Devices\'];
    set(handles.checkDevices,'Enable','on')
end
handles.checkPiButtons = [handles.check1];

%setting variable to be used in the email notification function
handles.statusLoop = 0;

load([handles.devicesFolder 'currentDevices.mat'])
handles.onDevice = onDevices{1};

% data location is given a variable
handles.dataLocation = [handles.devicesFolder,handles.onDevice,'\'];

% checks to see if the entered string is a valid device name by searching
% for the device folder
checkLocation = exist(handles.dataLocation);
if checkLocation == 0
    % pop up message box if the device name is invalid
    popup = msgbox('Invalid device name');
    
end
checkDevices_Callback(hObject, eventdata, handles);
plotData_Callback(hObject, eventdata, handles)

guidata(hObject, handles);

function varargout = plotAudioPilotBehavior_OutputFcn(hObject, eventdata, handles)

function plotData_Callback(hObject, eventdata, handles)
global stimulus

% loads the file, catch loop is in case the selected file is being saved
% simultaneously, the loop catch will pause and try to load again
if isempty(handles.performancefile)
    try
        load([handles.dataLocation,'performance.mat'])
    catch
        warndlg('Cannot load performance data')
        
    end
else
    try
        load(handles.performancefile)
    catch
        warndlg('Cannot load performance data')
        
    end
    
end

%displays the time at which the last trial was recorded
try
    set(handles.trialRecordText,'String',['Last Trial Recorded: ',timeStamp{size(timeStamp,1),2}])
catch
    set(handles.trialRecordText,'String','Last Trial Recorded: waiting for more data')
    
end

if phasechoice == 1
    set(handles.phaseDisplay,'String',['Phase',{'Shaping'}]);
elseif phasechoice == 2
    set(handles.phaseDisplay,'String',['Phase:',{'Detection'}]);
elseif phasechoice == 3
    set(handles.phaseDisplay,'String',['Phase:',{'Discrimination'}]);
end

%clear plots
delete(handles.graphPanel.Children(2:end))

% the text box for target tones displays which ones were
% selected for the associated device in the correct tab
set(handles.targetDisplay,'String',['Target:', {target}]);

% the text box for nontarget tones displays which ones were
% selected for the associated device in the correct tab
set(handles.nontargetDisplay,'String',['Nontarget:',{'none'}]);

%Get behavioral responses
responseVec = char(responseVec);

%%%%%  Trialwise Data  %%%%%
subplot(2,3,1:2,'Parent',handles.graphPanel);
cla
%Hits
H=zeros(length(responseVec),1);
%False Alarm
F=zeros(length(responseVec),1);
%Early
E=zeros(length(responseVec),1);
targetVec=0;
nonTargetVec=0;
for i = 1:length(responseVec)
    if strcmpi(responseVec(i),'H')
        H(i)=1;
    elseif strcmpi(responseVec(i),'F')
        F(i)=1;
    elseif strcmpi(responseVec(i),'E')
        E(i)=1;
    end
    if targetNontargetVec{i} == 'T'
        targetVec(i) = 1;
        nonTargetVec(i) = 0;
    else
        nonTargetVec(i) = 1;
        targetVec(i) = 0;
    end
end
numTar = sum(targetVec);
numNontar = sum(nonTargetVec);
plot(100*movmean(cumsum(H(max([1 length(H)-10]):end))./cumsum(targetVec(max([1 length(H)-10]):end))',1,'omitnan'),'b','linewidth',2);
hold on
plot(100*movmean(cumsum(F(max([1 length(F)-10]):end))./cumsum(nonTargetVec(max([1 length(F)-10]):end))',1,'omitnan'),'r','linewidth',2);
plot(100*movmean(cumsum(E(max([1 length(E)-10]):end))./[1:length(E(max([1 length(E)-10]):end))]',10,'omitnan'),'k','linewidth',2);
if length(responseVec)>1
    xlim([1 length(responseVec)])
end
aa=axis;
ylim([aa(3) aa(4)+5])
h=legend('H','F','E','autoupdate','off');
legend boxoff
xlabel('Trials')
ylabel('% Trials')
set(gca,'fontsize',8)
title('Trialwise Responses')

if ~strcmpi(stimulus,'LED')
    %Spectrum
    subplot(2,3,3,'Parent',handles.graphPanel);
    cla
    toneVec = cellfun(@str2num,toneVec);
    F = unique(toneVec)';
    Rf=[];
    for i = 1:length(F)
        f = find(toneVec==F(i));
        Rf(i)=sum(responseVec(f)=='H' | responseVec(f)=='F')./length(f);
        
    end
    plot(1:length(F),100*Rf,'k');
    hold on
    for i = 1:length(F)
        if sum(F(i)==target)>0
            plot(i,100*Rf(i),'bs','markerface','b')
        elseif sum(F(i)==nontarget)>0
            plot(i,100*Rf(i),'rs','markerface','r')
        else
            plot(i,100*Rf(i),'s','MarkerEdgeColor',[.5 .5 .5],'MarkerFaceColor',[.5 .5 .5])
        end
    end
    set(gca,'xtick',1:length(F),'xticklabel',F)
    if F(1) == 0
        set(gca,'xtick',1:length(F),'xticklabel',{'catch',F(2:end)})
    end
    xlabel('Frequency (kHz)')
    set(gca,'fontsize',8)
    title([{'Tone Responses'}])
    aa=axis;
    ylim([0 aa(4)])
end

% sets the graph's parent to be in the correct panel/tab
subplot(2,3,4,'Parent',handles.graphPanel);
cla

% plots response latency, aka first lick, axes are set
% for x axis to be from 0 to 4 seconds, and y axis set
% from 0 to 1 for relative response over total trials
L = lickResponseTarget/sum(lickResponseTarget);
LL = smooth(L,10);
area(xaxis,100*LL,'facecolor','b','facealpha',.5,'edgecolor','none');
hold on
L = lickResponseNonTarget/sum(lickResponseNonTarget);
LL = smooth(L,10);
area(xaxis,100*LL,'facecolor','r','facealpha',.5,'edgecolor','none');
aa=axis;
ylim([aa(3) aa(4)+5])
h=legend('T','NT','AutoUpdate','off');
legend boxoff
xlabel('Time(s)')
ylabel('% Licks')
xlim([0 4]);
hold on
title('Response Latency')
set(gca,'fontsize',8)

% box around sound
aa=axis;
x1 = [1 1];
y1 = [0 aa(4)]*.5;
x2 = [1 2];
y2 = [aa(4) aa(4)]*.5;
x3 = [2 2];
line(x1,y1,'Color','k','LineStyle','-','linewidth',2)
line(x2,y2,'Color','k','LineStyle','-','linewidth',2)
line(x3,y1,'Color','k','LineStyle','-','linewidth',2)

% sets the graph's parent to be in the correct panel/tab
subplot(2,3,6,'Parent',handles.graphPanel);
cla

% histogram of response types from animal
bar(1,100*earlyCount/totalTrials,'facecolor','none','edgecolor','k');
hold on
bar(2,100*hitCount/numTar,'facecolor','b','edgecolor','none');
bar(3,100*falseAlarmCount/numNontar,'facecolor','r','edgecolor','none');
set(gca,'xtick',1:3,'xticklabel',{'E','H','F'})
title('Response Rates')
set(gca,'fontsize',8)
ylabel('% Trials')

% sets the graph's parent to be in the correct panel/tab
subplot(2,3,5,'Parent',handles.graphPanel);
cla

% plots average lick response
if ~isempty(nansum(totalDataTarget(:)))
    L = nansum(totalDataTarget,1)./nansum(totalDataTarget(:));
    LL = smooth(L,10);
    area(xaxis,100*LL,'facecolor','b','facealpha',.5,'edgecolor','none');
    xlabel('Time(s)')
    xlim([0 4]);
    hold on
    title('Lick-o-gram')
    set(gca,'fontsize',8)
    aa=axis;
    ylim([aa(3) aa(4)])
end

if ~isempty(nansum(totalDataNonTarget(:)))
    L = nansum(totalDataNonTarget,1)./nansum(totalDataNonTarget(:));
    LL = smooth(L,10);
    area(xaxis,100*LL,'facecolor','r','facealpha',.5,'edgecolor','none');
    xlabel('Time(s)')
    xlim([0 4]);
    hold on
    title('Lick-o-gram')
    set(gca,'fontsize',8)
    aa=axis;
    ylim([aa(3) aa(4)])
end

ylabel('% Licks')

% box around sound
aa=axis;
x1 = [1 1];
y1 = [0 aa(4)]*.5;
x2 = [1 2];
y2 = [aa(4) aa(4)]*.5;
x3 = [2 2];
line(x1,y1,'Color','k','LineStyle','-','linewidth',2)
line(x2,y2,'Color','k','LineStyle','-','linewidth',2)
line(x3,y1,'Color','k','LineStyle','-','linewidth',2)

% immediately plots data
drawnow;

guidata(hObject, handles);

%checks status of the devices that are currently running on button press
function checkDevices_Callback(hObject, eventdata, handles)
global blockInterval

handles.failCheck = 0;

%for each device it first checks that the parameters have been moved into
%the associated folder and then looks at when the most recent trial was
%saved and then color codes the device based on the status
set(handles.checkPiButtons,'Enable','on')
loaded = 0;
while ~loaded
    if isempty(handles.performancefile)
        try
            load([handles.devicesFolder handles.onDevice '\performance.mat'])
            loaded = 1;
        end
    else
        try
            load(handles.performancefile)
            loaded = 1;
            set(handles.checkDevices,'Visible','off')
            set(handles.checkDevices,'Visible','off')
            set(handles.pauseButton,'Visible','off')
            set(handles.stopButton,'Visible','off')
            
        end
    end
end
set(handles.checkPiButtons,'String',handles.onDevice)

%Pause value
if handles.pauseStatus.Value
    pauseProgram=1;
else
    pauseProgram=0;
end

if isempty(handles.performancefile)
    try
        t1 = clock;
        t2 = datevec(timeStamp{size(timeStamp,1),2});
        timeCheck = (etime(t1,t2))/60;
        
        %if the device is not paused or on an inter block interval and has recorded a
        %trial within the last 15 minutes then the status is green
        if timeCheck < 15 && blockInterval == 0 && pauseProgram == 0
            set(handles.checkPiButtons,'BackgroundColor','g','ForegroundColor','k')
            
            %if the device is not paused or on an inter block interval and hasn't recorded a
            %trial within the last 15 minutes then the status is red
        elseif timeCheck > 15 && blockInterval == 0 && pauseProgram == 0
            set(handles.checkPiButtons,'BackgroundColor','r','ForegroundColor','k')
            
            %if the device is on an inter block interval then status is magenta
        elseif  blockInterval == 1 && pauseProgram == 0
            set(handles.checkPiButtons,'BackgroundColor','m','ForegroundColor','k')
            
            %if the device has been paused then the status is yellow
        elseif pauseProgram == 1
            set(handles.checkPiButtons,'BackgroundColor','y','ForegroundColor','k')
        end
        
    catch
        
        %if the parameters file has not been moved to the associated
        %device folder then the status is blue
        set(handles.checkPiButtons,'BackgroundColor','b','ForegroundColor','w')
    end
    
    %legend
    set(handles.waitStatus,'visible','on')
    set(handles.runStatus,'visible','on')
    set(handles.blockStatus,'visible','on')
    set(handles.failStatus,'visible','on')
    set(handles.pauseStatus,'visible','on')
    
end

guidata(hObject, handles);

%graphs data from most recent/current file for the device by populating the
%information into the file selection drop down menus and running the graph
%function
function check1_Callback(hObject, eventdata, handles)
if length(handles.check1.String) > 4
    delete(handles.graphPanel.Children(2:end))
    set(handles.trialRecordText,'String','')
    
    % data location is given a variable
    handles.dataLocation = [handles.devicesFolder,handles.deviceChoice.String,'/'];
    
    handles.listFolder = dir(handles.dataLocation);
    validFiles = find(~[handles.listFolder.isdir]);
    fileDates = [handles.listFolder.datenum];
    [~,sortedFiles] = sort(fileDates,'descend');
    sortedFiles = sortedFiles(ismember(sortedFiles,validFiles));
    textDisplay = {};
    for z = 1:numel(sortedFiles)
        if handles.listFolder(sortedFiles(z)).name(2) == 'e'
            textDisplay = [textDisplay;{handles.listFolder(sortedFiles(z)).name}];
        end
    end
    handles.fileSelection.String = textDisplay;
    set(handles.fileSelection,'Enable','on')
    set(handles.plotData,'Enable','on')
    graphButton_Callback(hObject, eventdata, handles)
end
guidata(hObject, handles);

function fileLocation_Callback(hObject, eventdata, handles)
function fileLocation_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function stopButton_Callback(hObject, eventdata, handles)
global quit intervalStop
if handles.stopButton.Value
    quit = 1;
    intervalStop = 1;
    handles.stopButton.String = 'Done.';
    set(handles.stopButton,'Enable','off')
    set(handles.pauseButton,'visible','off')
else
    handles.stopButton.String = 'Stop';
    set(handles.stopButton,'Enable','on')
    set(handles.pauseButton,'visible','on')
    
end
guidata(hObject, handles);

function pauseButton_Callback(hObject, eventdata, handles)
global paused

if handles.pauseButton.Value
    handles.pauseStatus.Value=1;
    handles.pauseButton.String = 'Unpause';
    paused = 1;
    set(handles.stopButton,'Enable','off')
else
    handles.pauseStatus.Value=0;
    handles.pauseButton.String = 'Pause';
    paused = 0;
    set(handles.stopButton,'Enable','on')
end
guidata(hObject, handles);
checkDevices_Callback(hObject, eventdata, handles)

guidata(hObject, handles);

