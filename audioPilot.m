% audioPilot is a Matlab program for controlling behavioral
% experiments using Raspberry Pi hardware. See documentation
% for installation and operational details. Nikolas A. Francis 2021.

%Initialize global variables
global quit intervalStop pauseProgram blockInterval trialRunning paused stimulus phaseChoice devicesFolder

%Run GUI for setting parameters
audioPilotGUI

%Start behavioral training program
audioPilotBehavior

%clear connection to Raspberry Pi
warning('off','shared_linuxservices:nanoreqclient:receiveTimeout')
clearRPI=0;
while clearRPI==0
    try
        system(rpi,'sudo reboot');
    catch
        clear rpi
        clearRPI=1;
    end
end